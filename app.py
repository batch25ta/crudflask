from crud.crudflask.models.anggota import database
from flask import Flask,jsonify, request
import json

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False


@app.route("/")
def main():
    return "Welcome!"

@app.route("/show")
def showUsers():
    dbresult = mysqldb.showUsers()
    result = []
    for items in dbresult:
        user = {
            "id" : items[0],
            "nama" : items[1],
            "alamat" : items[2],
            "no_tlp" : items[3]            
        }
        result.append(user)
    return jsonify(result)

@app.route("/showid", methods=["POST"])
def showUser():
    params = request.json
    dbresult = mysqldb.showUserById(**params)
    user = {
        "id" : dbresult[0],
        "nama" : dbresult[1],
        "alamat" : dbresult[2],
        "no_tlp" : dbresult[3]            
    } 
    return jsonify(user) 

@app.route("/add",methods=["POST"])
def addUser():
    params = request.json
    result = mysqldb.insertUser(**params)
    user = {
        "id" : result[0],
        "nama" : result[1],
        "alamat" : result[2],
        "no_tlp" : result[3]            
    } 
    return jsonify(user) 

@app.route("/updateid", methods=["PUT"])
def updateId():
    params = request.json
    dbresult = mysqldb.updateUserById(**params)
    data = {
        "id":dbresult[0],
        "nama":dbresult[1],
        "alamat":dbresult[2],
        "no_tlp":dbresult[3]         
    }
    return jsonify(data) 

@app.route("/deleteid", methods=["DELETE"])
def deleteId():
    params = request.json
    result = mysqldb.deleteUserById(**params)
    return jsonify(result)

if __name__ == "__main__":
    mysqldb = database()
    if mysqldb.db.is_connected():
        print('Connected to MySQL database')
    
    
    app.run(debug=True)
    
    if mysqldb.db is not None and mysqldb.db.is_connected():
        mysqldb.db.close()