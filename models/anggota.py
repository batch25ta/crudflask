from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='dbkoperasi',
                              user='root',
                              password='>!!demo22!!<')
        except Exception as e:
            print(e)
    
    def showUsers(self):
        try:  
            cursor = self.db.cursor()
            query ='''select * from anggota'''
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Exception as e:
            print(e)
    
    def showUserById(self, **params):
        try:
            cursor = self.db.cursor()
            query = '''
                select * 
                from anggota
                where id = {0};
            '''.format(params["id"])
            
            cursor.execute(query)
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
    
    def insertUser(self, **params):
        try:
            column = ', '.join(list(params['values'].keys()))
            values = tuple(list(params['values'].values()))
            crud_query = '''insert into anggota ({0}) values {1};'''.format(column, values)
            #print(crud_query)
            cursor = self.db.cursor()
            cursor.execute(crud_query)
            print("Data berhasil ditambahkan!")
        except Exception as e:
            print(e)
    
    def updateUserById(self, **params):
        try:
            userid = params['id']
            values = self.restructureParams(**params['values'])
            crud_query = '''update anggota set {0} where id = {1};'''.format(values, userid)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            print("Data berhasil diupdate!")
        except Exception as e:
            print(e)
    
    def deleteUserById(self, **params):
        try:
            userid = params['id']
            crud_query = '''delete from customers where id = {0};'''.format(userid)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            print("Data berhasil dihapus!")
        except Exception as e:
            print(e)
    
    def dataCommit(self):
        self.db.commit()
        
    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result